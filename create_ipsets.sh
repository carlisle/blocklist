#!/usr/bin/env sh

COUNTRY_TLD=""
BLOCKLIST=""

declare -a COUNTRIES=(

kp	# North Korea
sy	# Syria
ir	# Iran
cu	# Cuba
cn      # China
ru	# Russia
ua	# Ukraine
by	# Belarus

)


####

for COUNTRY_TLD in "${COUNTRIES[@]}"; do 

  BLOCKLIST="${COUNTRY_TLD}-aggregated.zone"

# download the latest ipv4 list, the previous one will be renamed with .1
# for ipv6 lists see https://www.ipdeny.com/ipv6/ipaddresses/blocks/

  wget --backups=1 https://www.ipdeny.com/ipblocks/data/aggregated/${BLOCKLIST}

  if [ -f ${BLOCKLIST} ]; then

    IPSET_LIST="load_ipset_${COUNTRY_TLD}.sh"

    printf "# Run this script to load an ipset for IPSET_${COUNTRY_TLD}\n" > ${IPSET_LIST}
    printf "# run ipset list -terse to see current sets\n"
    printf "# first time only\n"
    printf "ipset create IPSET_${COUNTRY_TLD} hash:net\n" >> ${IPSET_LIST}
    printf "# may want to run ipset flush IPSET_${COUNTRY_TLD}\n"

    IPLIST=$(grep -Ev "^#" ${BLOCKLIST})
    for IP in $IPLIST
    do
      printf "ipset add IPSET_${COUNTRY_TLD} ${IP}\n" >> ${IPSET_LIST}
    done

    IPTABLES_FILE="iptables_${COUNTRY_TLD}_ipset.sh"

    printf "# Add these lines to /etc/sysconfig/iptables to use the ipset\n" > ${IPTABLES_FILE}

    printf "# Block incoming traffic from ip in IPSET_${COUNTRY_TLD}\n" >> ${IPTABLES_FILE}
    printf "%s\n" \
      "-A INPUT   -m set --match-set IPSET_${COUNTRY_TLD} src -j DROP"  >> ${IPTABLES_FILE}

    printf "# Block forwared traffice from ip in IPSET_${COUNTRY_TLD}\n"  >> ${IPTABLES_FILE}
    printf "%s\n" \
      "-A FORWARD -m set --match-set IPSET_${COUNTRY_TLD} src -j DROP"  >> ${IPTABLES_FILE}

    printf "# Log and block outbound traffic to ip in IPSET_${COUNTRY_TLD}\n" >> ${IPTABLES_FILE}
    printf "%s\n" \
      "-A OUTPUT  -m set --match-set IPSET_${COUNTRY_TLD} dst -m limit --limit 5/m --limit-burst 20 \
 -j LOG --log-prefix \" IPv4 OUTPUT IPSET_${COUNTRY_TLD}\" --log-level debug" >> ${IPTABLES_FILE}
    printf "%s\n" \
      "-A OUTPUT  -m set --match-set IPSET_${COUNTRY_TLD} dst -j DROP"  >> ${IPTABLES_FILE}

  fi

done

